# License Hint

Copyright © 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.

This work is licensed under the [MIT](LICENSES/MIT.txt) license.

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
