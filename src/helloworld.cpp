// Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
//
// SPDX-License-Identifier: MIT

#include <string>
#include <cstdlib>

#include "src/helloworld.h"

std::string getUserNameViaEnv() {
    // Return content of the variable GITLAB_USER_NAME
    const char* gitlabUserName = std::getenv("GITLAB_USER_NAME");
    if (gitlabUserName == nullptr)
        return std::string{"World"};
    return std::string{gitlabUserName};
}
